/** Draggable icon **/
var icons = document.getElementsByClassName('icon');
var iconIsDrag = false;
var iconDownX, iconDownY;
var currentIcon;

for (var icon = 0; icon < icons.length; icon++) {
	icons[icon].onmousedown = function(event) {
		currentIcon = this;
		currentIcon.className += ' active';

		iconIsDrag = true;

		iconDownX = event.layerX;
		iconDownY = event.layerY;


		window.onmousemove = function(event) {
			if (iconIsDrag) {
				currentIcon.style.left = event.clientX - iconDownX + 'px';
				currentIcon.style.top = event.clientY - iconDownY + 'px';
			}
		}

		window.onmouseup = function(event) {
			if (iconIsDrag) {
				currentIcon.className = 'icon';
				currentIcon = null;
				iconIsDrag = false;
			}
		}
	}
}