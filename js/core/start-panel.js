
function getDateTime() {
	var date = new Date();

	var options = {
		hour: 'numeric',
		minute: 'numeric',
		year: 'numeric',
		month: 'numeric',
		day: 'numeric',
	};

	return date.toLocaleString("ru", options);
}

var panel_date_time = document.getElementById('panel-date-time');
panel_date_time.textContent = getDateTime();