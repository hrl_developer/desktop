/** Draggable window **/
var windows = document.getElementsByClassName('window-panel');
var windowIsDrag = false;
var windowDownX, windowDownY;
var currentWindow;

for (var window_ = 0; window_ < windows.length; window_++) {
	windows[window_].onmousedown = function(event) {

		Array.from(event.path).forEach(result => {
			if (result.className == 'window') {
				currentWindow = result;

				windowIsDrag = true;

				windowDownX = event.layerX;
				windowDownY = event.layerY;
			}
		});

		window.onmousemove = function(event) {
			if (windowIsDrag) {
				currentWindow.style.left = event.clientX - windowDownX + 'px';
				currentWindow.style.top = event.clientY - windowDownY + 'px';
			}
		}

		window.onmouseup = function(event) {
			if (windowIsDrag) {
				currentWindow = null;
				windowIsDrag = false;
			}
		}
	}	
}